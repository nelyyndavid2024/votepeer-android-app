package info.bitcoinunlimited.voting

import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import io.mockk.clearAllMocks
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before

@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class SplashScreenActivityTest {

    private lateinit var scenario: ActivityScenario<SplashScreenActivity>

    @Before
    fun setupTest() = runBlockingTest {
        scenario = launchActivity()
    }

    @After
    fun clear() = runBlockingTest {
        scenario.moveToState(Lifecycle.State.DESTROYED)
        clearAllMocks()
    }

//    @Test
//    fun verifyUi() = runBlockingTest {
//        onView(withId(R.id.splash_screen_title))
//            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
//        onView(withId(R.id.splash_screen_title))
//            .check(matches(withText("VotePeer: On-chain elections.")))
//        onView(withId(R.id.splash_screen_progress_bar))
//            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
//        onView(withId(R.id.bitcoin_unlimited))
//            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
//    }
}
