package info.bitcoinunlimited.voting.resources

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.* // ktlint-disable no-wildcard-imports
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import info.bitcoinunlimited.votepeer.R

@Composable
fun ResourcesScreen(
    openWebsite: (url: String) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.colorWhite))
            .padding(16.dp)
    ) {
        Text(
            text = "VotePeer",
            fontWeight = FontWeight.Bold,
            color = Color.DarkGray,
            modifier = Modifier.align(Alignment.CenterHorizontally).padding(8.dp),
            fontSize = 24.sp,
        )

        Button(
            onClick = {
                openWebsite("https://voter.cash")
            },
        ) {
            Text(
                text = "Election manager",
                textAlign = TextAlign.Center,
                modifier = Modifier.align(Alignment.Bottom).fillMaxWidth(),
            )
        }

        Button(
            onClick = {
                openWebsite("https://docs.voter.cash/")
            },
        ) {
            Text(
                text = "Documentation & Research",
                textAlign = TextAlign.Center,
                modifier = Modifier.align(Alignment.Bottom).fillMaxWidth(),
            )
        }

        Button(
            onClick = {
                openWebsite("https://t.me/buip129/")
            },
        ) {
            Text(
                text = "Telegram",
                textAlign = TextAlign.Center,
                modifier = Modifier.align(Alignment.Bottom).fillMaxWidth(),
            )
        }

        Button(
            onClick = {
                openWebsite("https://twitter.com/votepeer")
            },
        ) {
            Text(
                text = "Twitter",
                textAlign = TextAlign.Center,
                modifier = Modifier.align(Alignment.Bottom).fillMaxWidth(),
            )
        }

        Button(
            onClick = {
                openWebsite("https://old.reddit.com/r/btc/comments/itvski/announcing_votepeer_from_bu_decentralised_voting/")
            },
        ) {
            Text(
                text = "Reddit: Announcement & discussion",
                textAlign = TextAlign.Center,
                modifier = Modifier.align(Alignment.Bottom).fillMaxWidth(),
            )
        }

        Button(
            onClick = {
                openWebsite("https://www.bitcoinunlimited.info")
            },
        ) {
            Text(
                text = "Bitcoin Unlimited",
                textAlign = TextAlign.Center,
                modifier = Modifier.align(Alignment.Bottom).fillMaxWidth(),
            )
        }

        Text(
            text = "In the news",
            fontWeight = FontWeight.Bold,
            color = Color.DarkGray,
            modifier = Modifier.align(Alignment.CenterHorizontally).padding(8.dp),
            fontSize = 24.sp,
        )

        Button(
            onClick = {
                openWebsite("https://read.cash/@Bitcoin-Unlimited/announcing-votepeer-from-bu-767d16a3")
            },
        ) {
            Text(
                text = "Read.cash - Announcing VotePeer from BU",
                textAlign = TextAlign.Center,
                modifier = Modifier.align(Alignment.Bottom).fillMaxWidth(),
            )
        }

        Button(
            onClick = {
                openWebsite("https://news.bitcoin.com/bitcoin-unlimited-launches-two-option-voting-app-powered-by-bitcoin-cash/")
            },
        ) {
            Text(
                text = "Bitcoin Unlimited Launches Two-Option Voting App Powered by Bitcoin Cash",
                textAlign = TextAlign.Center,
                modifier = Modifier.align(Alignment.Bottom).fillMaxWidth(),
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun FundExternalScreenPreview() {
    ResourcesScreen() {
    }
}
